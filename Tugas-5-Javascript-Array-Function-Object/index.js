// soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var sort = daftarHewan.sort()

for(var i = 0; i < daftarHewan.length; i++){
    console.log(sort[i])
}

// soal 2

function introduce(data){
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

// soal 3

function hitung_huruf_vokal(kata){
    var jumlahVokal = 0;
    for(var i = 0; i < kata.length; i++){
        if(kata.charAt(i) == "a" || kata.charAt(i) == "i" || kata.charAt(i) == "u" || kata.charAt(i) == "e" || kata.charAt(i) == "o" || kata.charAt(i) == "A" || kata.charAt(i) == "I" || kata.charAt(i) == "U" || kata.charAt(i) == "E" || kata.charAt(i) == "O" ){
            jumlahVokal = jumlahVokal + 1;
        }
    }
    return jumlahVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// soal 4

function hitung(nilaiKelipatan){
    var nilaiAwal = -2
    for(var i = 0; i < nilaiKelipatan; i++){
        nilaiAwal = nilaiAwal + 2
    }
    return nilaiAwal
}

console.log( hitung(0) ) 
console.log( hitung(1) ) 
console.log( hitung(2) ) 
console.log( hitung(3) ) 
console.log( hitung(5) ) 




