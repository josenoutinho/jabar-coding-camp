// soal 1

var nilai = 90

if(nilai >= 85){
    console.log("indeks : A")
}else if(nilai >= 75 && nilai < 85){
    console.log("indeks : B")
}else if(nilai >= 65 && nilai < 75){
    console.log("indeks : C")
}else if(nilai >= 55 && nilai < 65){
    console.log("indeks : D")
}else if(nilai < 55){
    console.log("indeks : E")
}

// soal 2

var tanggal = 22
var bulan = 7
var tahun = 2020

switch (bulan) {
    case 1: { bulan = "Januari"; break; }
    case 2: { bulan = "Februari"; break; }
    case 3: { bulan = "Maret"; break; }
    case 4: { bulan = "April"; break; }
    case 5: { bulan = "Mei"; break; }
    case 6: { bulan = "Juni"; break; }
    case 7: { bulan = "Juli"; break; }
    case 8: { bulan = "Agustus"; break; }
    case 9: { bulan = "September"; break; }
    case 10: { bulan = "Oktober"; break; }
    case 11: { bulan = "November" ;break; }
    case 12: { bulan = "Desember"; break; }
}

console.log(tanggal + " " + bulan + " " + tahun)

// soal 3

n = 7
for(var i = 1; i <= n; i++){
    var tag = ''
    for(var j = 1; j<= i; j++){
        tag = tag + '#'
    }
    console.log(tag)
}

// soal 4

m = 7;
for(var i = 1; i <= m; i++){
    if(i % 3 == 1){
        console.log(i + " - I love programming")
    }else if(i % 3 == 2){
        console.log(i + " - I love Javascript")
    }else if(i % 3 == 0){
        console.log(i + " - I love VueJS")
        console.log("===")
    }
}