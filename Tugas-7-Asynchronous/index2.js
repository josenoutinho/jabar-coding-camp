var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// soal 2

readBooksPromise(10000,books[0])
    .then((response) => readBooksPromise(response,books[1])
        .then((response) => readBooksPromise(response,books[2])
            .then((response) => readBooksPromise(response,books[3])        
            ).catch((response) => { return response })
        ).catch((response) => { return response })
    ).catch((response) => { return response })