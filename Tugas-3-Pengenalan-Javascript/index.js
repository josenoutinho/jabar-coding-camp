// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var saya = pertama.substr(0,4);
var senang = pertama.substr(12,6);
var belajar = kedua.substr(0,7);
var javascript = kedua.substr(8,10).toUpperCase();

var hasilSoalPertama = saya + " " + senang + " " + belajar + " " + javascript;
console.log(hasilSoalPertama);

// soal 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var hasilSoalKedua = ( parseInt(kataPertama) - parseInt(kataKedua) - parseInt(kataKetiga) ) * parseInt(kataKeempat);
console.log(hasilSoalKedua);

// soal 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0,3);
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25);

console.log('kataPertama: ' + kataPertama);
console.log('kataKedua: ' + kataKedua);
console.log('kataKetiga: ' + kataKetiga);
console.log('kataKeempat: ' + kataKeempat);
console.log('kataKelima: ' + kataKelima);
